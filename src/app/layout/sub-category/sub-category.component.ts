import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
  
  p: Number = 1;
  subCategoryArr : any=[];
  categoryArr : any = [];
  category :any;

  loading = true;

  constructor(private rest :RestService,private route : Router) { }

  edit(item)
  {
    this.route.navigate(['/layout/sub-category-form'], {queryParams : item});
  }
  
  delete(item)
  {
    // this.rest.deleteSubCategory(item).then(success =>{
    //   console.log(success);
    // }).catch(err => {
    //   console.log(err);
    // });

    Swal.fire({
      title: 'Updated Successfully!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
          if (result.value) {
            this.rest.deleteSubCategory(item).then(success => {
          console.log(success);
          Swal.fire(
            'Deleted!',
            // 'Your imaginary file has been deleted.',
            'success'
          )
        }).catch(err => {
          console.log(err);
        });
        
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
    })
  }

  ngOnInit() {

   this.getCategory();
  }

  getCategory() {
    this.rest.getCategory().subscribe(data => {
      this.categoryArr = this.rest.snapShotToArray(data);
      this.loading = false;
    });
  }

  selectCategory() {
    this.loading = true;
    console.log(this.category);
    this.rest.getSubCategory(this.category).subscribe(data => {
      this.subCategoryArr = this.rest.snapShotToArray(data);
      this.loading = false;
    });
  }
}
