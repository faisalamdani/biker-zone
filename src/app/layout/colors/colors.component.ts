import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {
  colorArr :any = [];
  p: Number = 1;

  loading = true;
  constructor(private rest :RestService,private route : Router) { }

  edit(item)
  {
    this.route.navigate(['/layout/colors-form'], {queryParams : item});
  }
  
  delete(item)
  {
    // this.rest.deleteColors(item).then(success =>{
    //   console.log(success);
    // }).catch(err => {
    //   console.log(err);
    // });

    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
          if (result.value) {
            this.rest.deleteColors(item).then(success => {
          console.log(success);
          Swal.fire(
            'Deleted!',
            // 'Your imaginary file has been deleted.',
            'success'
          )
        }).catch(err => {
          console.log(err);
        });
        
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
    })
  }

  ngOnInit() {
    this.rest.getColors().subscribe(data => {
      this.colorArr = this.rest.snapShotToArray(data);
      this.loading = false;
    });
  }

}
