import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {RestService} from '../../rest.service';
import {Router,ActivatedRoute} from '@angular/router';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';

import { finalize } from 'rxjs/operators';
import { Key } from 'protractor';
import Swal from 'sweetalert2';

export interface ProductCategory {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})


export class ProductFormComponent implements OnInit {
  params:any = [];
  isLinear = false;
  files: File[] = [];
  categoryArr :any =[];
  subCategoryArr: any = [];
  colorArr : any = [];
  sizeArr: any = [];
  productKey :any = null;
  descArr : any = [];
  detailsArr : any =[];
  images : any =[];
  
  loading = false;


  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadPercent: any;
  downloadURL: any;
  
  constructor(private rest : RestService,private router : ActivatedRoute,private route :Router, private afs: AngularFireStorage) { }

  selectedValue: string;

  // category : any = ["Two-wheeler","Three-wheeler","Four-wheeler"];
  // subCategory : any = ["Bike","Auto","Car"];
  // product: ProductCategory[] = [
  //   {value: 'two-wheeler', viewValue: 'Bike'},
  //   {value: 'three-wheeler', viewValue: 'Auto'},
  //   {value: 'four-wheeler', viewValue: 'Cars'}
  // ];

  basic = new FormGroup({
    // firstCtrl : new FormControl('', Validators.required),
    pname : new FormControl('', Validators.required),
    pdname : new FormControl('', Validators.required),
    category : new FormControl('',Validators.required),
    subcategory : new FormControl('',Validators.required),
    cost : new FormControl('', Validators.required),
    discount : new FormControl('', Validators.required),
    key : new FormControl()  
  });

  description = new FormGroup ({
    shortDescription : new FormControl('', Validators.required),
    longDescription : new FormControl('', Validators.required)
  });

  details = new FormGroup({
    color : new FormControl('', Validators.required),
    size : new FormControl('', Validators.required),
    brand : new FormControl('',Validators.required),
    movement : new FormControl('',Validators.required),
    voltage : new FormControl('',Validators.required),
    brightness : new FormControl('',Validators.required),
    power : new FormControl('',Validators.required),
    warranty : new FormControl('',Validators.required),
    key : new FormControl()
  });

  imageForm = new FormGroup({
    imgs : new FormControl('')
  });

  getCategory() {
    this.rest.getCategory().subscribe(data => {
      this.categoryArr = this.rest.snapShotToArray(data);
      console.log(this.categoryArr);
    });
  }

  getColors()
  {
    this.rest.getColors().subscribe(data => {
      this.colorArr = this.rest.snapShotToArray(data);
      console.log(this.colorArr);
    });
  }

  getSizes()
  {
    this.rest.getSizes().subscribe(data => {
      this.sizeArr = this.rest.snapShotToArray(data);
      console.log(this.sizeArr);
    });
  }

  saveBasic() {
    // this.rest.addProductBasic(this.basic.value).then(success => {
    //   console.log(success);
    //   this.productKey = success.key;
    // }).catch(err => {

    // });
    this.loading = true;
    let key = this.basic.controls.key.value;
    console.log(key);
    if(key === null)
    {
      this.addBasic();
    }
    else
    {
      this.updateBasic();
    }

  }

  addBasic()
  {
    
    this.rest.addProductBasic(this.basic.value).then(success =>{
      console.log(success);
      this.productKey = success.key;
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }
  updateBasic()
  {
    this.rest.editProductBasic(this.basic.value).then(success => {
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }


  saveDesc() {
    console.log(this.productKey);
    this.loading = true;
    this.rest.addProductDesc(this.description.value,this.productKey).then(success => {
      console.log(success);
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  getProductDesc() {
    this.rest.getProductDesc(this.productKey).subscribe(data => {
      this.descArr = this.rest.snapShotToArray(data);
      console.log(this.descArr);
    });
  }

  addProductDesc() {
    this.rest.editProductDesc(this.productKey, this.description.value).then(success =>{
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  updateProductDetails() {

  }

  // getProductDetails() {
   
  // }

  saveDetails() {
    this.loading = true;
    this.rest.addProductDetails(this.details.value,this.productKey).then(success => {
      console.log(success);
      this.loading =false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  selectCategory() {
    console.log(this.subCategoryArr)

    // console.log("***********************");

    let category = this.basic.controls.category.value;
    this.rest.getSubCategory(category).subscribe(data => {
      this.subCategoryArr = this.rest.snapShotToArray(data);
      console.log(this.subCategoryArr)
    });
  }
b
  selectSubCategory(catval) {
    console.log(this.subCategoryArr)

    // console.log("@@@@@@@@@@@@@@");

    // let category = this.basic.controls.category.value;
    this.rest.getSubCategory(catval).subscribe(data => {
      this.subCategoryArr = this.rest.snapShotToArray(data);
      console.log(this.subCategoryArr)
      this.basic.value.subcategory = this.params.subcategory;
    });
  }

  uploadImages(event) {
    this.loading = true;
    console.log(event);
    // const id = Math.random().toString(36).substring(2);
    // this.ref = this.afs.ref(id);
    // this.task = this.ref.put(event.target.files[0]);

    let file = event.target.files[0];
    console.log(file);
    let path = `posts/${file.name}`;
    // if (file.type.split('/')[0] !== 'image') {
    //   return alert('Error');
    // }
    // else {
      let ref = this.afs.ref(path);
      let task = this.afs.upload(path, file);
      this.uploadPercent = task.percentageChanges();
      console.log('Image chargée avec succès');
      task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = ref.getDownloadURL();
          this.downloadURL.subscribe(url => {
            console.log(url);
            let obj : any = {};
            obj.url = url;
            this.rest.addImages(obj,this.productKey);
            this.loading = false;
          });
        })
      ).subscribe();
    // }
}

getImages()
{
  this.rest.getImages(this.productKey).subscribe(data => {
    this.images = this.rest.snapShotToArray(data);
    console.log(this.images);
  });
}

deleteImg(src)
{
  this.rest.deleteImg(this.productKey, src).then(success => {
    console.log(success);
  }).catch(err => {
    console.log(err);
  });
}
  srcKey(productKey: any, srcKey: any) {
    throw new Error("Method not implemented.");
  }

  ngOnInit() {
    this.getCategory();
    this.getColors();
    this.getSizes();

    this.router.queryParams.subscribe(params => {
      console.log(params);
      if(params.hasOwnProperty('key'))
      {
    //     this.params = params;
    //     this.basic.patchValue(params);
    //     console.log(this.basic.value);
    //     this.selectSubCategory(this.params.category);
    //     this.productKey = params.key;

    //  this.rest.getProductDetails(this.productKey).subscribe(detailsObj => {
    //     // let detailsObj :any = this.rest.snapShotToArray(data);
    //     console.log(detailsObj);
    //     if(detailsObj != null)
    //     this.details.patchValue(detailsObj);

    //     this.rest.getProductDesc(this.productKey).subscribe(descObj => {
    //     // let descObj :any = this.rest.snapShotToArray(data);
    //     console.log(descObj);
    //     if(descObj != null)
    //     this.description.patchValue(descObj);

    //     this.rest.getImages(this.productKey).subscribe(data => {
    //       this.images = this.rest.snapShotToArray(data);
    //       // this.saveAlert();
    //       console.log(this.images);
    //     });


    //     });
    //   });
          this.rest.getProductByKey(params.key).subscribe(data => {
            let productObj :any = data;
            productObj.key = params.key;
            this.selectSubCategory(productObj.category);
                this.productKey = params.key;
            this.basic.patchValue(productObj);
            if(productObj.hasOwnProperty('description'))
              this.description.patchValue(productObj.description);
            if(productObj.hasOwnProperty('details'))
              this.details.patchValue(productObj.details);
            if(productObj.hasOwnProperty('images'))
              {
                this.rest.getImages(this.productKey).subscribe(imgs => {
                        this.images = this.rest.snapShotToArray(imgs);
                        console.log(this.images);
                      });
              }
              
          });
        console.log(params);
      }
      

      // this.router.queryParams.subscribe(params => {
      //   this.basic.patchValue(params);
      // });
    
    });
    // this.save();
  
  }

  saveAlert() {
    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok!',
    }).then((result) => {
      // this.route.navigate(['/layout/product']);

    })  }

}
