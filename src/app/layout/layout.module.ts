import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { CategoryComponent } from './category/category.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { SubCategoryFormComponent } from './sub-category-form/sub-category-form.component';
import {  MatInputModule, MatButtonModule } from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import { ColorsComponent } from './colors/colors.component';
import { ColorsFormComponent } from './colors-form/colors-form.component';
import { SizeFormComponent } from './size-form/size-form.component';
import { SizeComponent } from './size/size.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxLoadingModule } from 'ngx-loading';
import {MatStepperModule} from '@angular/material/stepper';




const routes: Routes =[
  {
    path : 'category',
    component : CategoryComponent
  },
  {
    path : 'category-form',
    component : CategoryFormComponent 
  },

  {
    path: 'sub-category',
    component : SubCategoryComponent,
  },
  {
    path : 'sub-category-form',
    component : SubCategoryFormComponent
  },

  {
    path : 'product',
    component : ProductComponent
  },
  {
    path : 'product-form',
    component : ProductFormComponent
  },
  {
    path : 'colors',
    component : ColorsComponent
  },
  {
    path : 'colors-form',
    component : ColorsFormComponent
  },
  {
    path : 'size',
    component : SizeComponent
  },
  {
    path : 'size-form',
    component : SizeFormComponent
  }
]
@NgModule({
  declarations: [ProductComponent, ProductFormComponent, CategoryComponent, CategoryFormComponent, SubCategoryComponent, SubCategoryFormComponent, ColorsComponent, ColorsFormComponent, SizeFormComponent, SizeComponent],
  imports: [
    CommonModule,
    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatSelectModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxLoadingModule,
    RouterModule.forChild(routes)
  ]
})
export class LayoutModule { }
