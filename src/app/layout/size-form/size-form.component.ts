import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {RestService} from '../../rest.service';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-size-form',
  templateUrl: './size-form.component.html',
  styleUrls: ['./size-form.component.scss']
})
export class SizeFormComponent implements OnInit {
  sizeForm = new FormGroup({
    size : new FormControl('',Validators.required),
    key : new FormControl()
  });

  loading = false;
  constructor(private rest : RestService,private router : ActivatedRoute,private route :Router) { }

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.sizeForm.patchValue(params);
    });
  }

  save() {
    this.loading = true;
      let key = this.sizeForm.controls.key.value;
      if(key === null)
      {
        this.add();
      }
      else {
        this.update();
      }

  }

  add() {
    this.rest.addSize(this.sizeForm.value).then(success => {
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  update() {
    this.rest.editSize(this.sizeForm.value).then(success => {
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });

  }

  saveAlert() {
    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok!',
    }).then((result) => {
      this.route.navigate(['/layout/size']);

    })  }

}
