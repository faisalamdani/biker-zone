import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  constructor(private rest : RestService,private router : ActivatedRoute,private route :Router) { }

  loading = false;

  categoryForm = new FormGroup({
    name : new FormControl('',Validators.required),
    key : new FormControl()
  });

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.categoryForm.patchValue(params);
      console.log(params);
    });
  }
  save()
  {
    this.loading = true;
    let key = this.categoryForm.controls.key.value;
    if(key === null)
    {
      this.add();
    }
    else
    {
      this.update();
    }

  }
  add()
  {
    this.rest.addCategory(this.categoryForm.value).then(success =>{
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  update()
  {
    this.rest.editCategory(this.categoryForm.value).then(success => {
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  saveAlert() {
    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok!',
    }).then((result) => {
      this.route.navigate(['/layout/category']);

    })  }


}
