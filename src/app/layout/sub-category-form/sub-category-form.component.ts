import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {RestService} from '../../rest.service';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-category-form',
  templateUrl: './sub-category-form.component.html',
  styleUrls: ['./sub-category-form.component.scss']
})
export class SubCategoryFormComponent implements OnInit {
  categoryArr :any =[];
  loading = false;

  constructor(private rest : RestService,private router : ActivatedRoute,private route :Router) { }

  subCategoryForm = new FormGroup({
    name : new FormControl('', Validators.required),
    category : new FormControl('',Validators.required),
    key : new FormControl()
  });

  save()
  {
    this.loading = true;
    let key = this.subCategoryForm.controls.key.value;
    if(key === null)
    {
      this.add();
    }
    else
    {
      this.update();
    }

  }
  add()
  {
   let obj : any = {};
   obj.name = this.subCategoryForm.controls.name.value;
   let category :any = this.subCategoryForm.controls.category.value;
   console.log(category);
    this.rest.addSubCategory(obj,category).then(success =>{
      this.loading = false;
    this.saveAlert();
    }).catch(err => {

    });
  }

  update()
  {
    let obj : any = {};
    obj.key = this.subCategoryForm.controls.key.value;
    obj.category = this.subCategoryForm.controls.category.value;
    obj.name = this.subCategoryForm.controls.name.value;
    this.rest.editSubCategory(obj).then(success => {
      this.loading = false;
    this.saveAlert();

    }).catch(err => {

    });
  }

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.subCategoryForm.patchValue(params);
      console.log(params);
      this.getCategory();
    });
  }

  getCategory() {
    this.rest.getCategory().subscribe(data => {
      this.categoryArr = this.rest.snapShotToArray(data);
      console.log(this.categoryArr);
    });
  }

  saveAlert() {
    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok!',
    }).then((result) => {
      this.route.navigate(['/layout/sub-category']);

    })  }
}
