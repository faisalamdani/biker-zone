import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  p: Number = 1;
  // count: Number = 5;

  productArr :any = [];
  loading = true;
  constructor(private rest :RestService,private route : Router) { }

  edit(item) {
    let obj :any = {};
    obj.key = item.key
    this.route.navigate(['/layout/product-form'],{queryParams : obj});
  }

delete(item) {
  // this.rest.deleteProduct(item).then(success => {
  //   console.log(success);
  // }).catch(err => {
  //   console.log(err);
  // });

  Swal.fire({
    title: 'Successfully Deleted!',
    // text: 'You will not be able to recover this imaginary file!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
        if (result.value) {
          this.rest.deleteProduct(item).then(success => {
            console.log(result);
        console.log(success);
        this.getProduct();
        Swal.fire(
          'Deleted!',
          // 'Your imaginary file has been deleted.',
          'success'
        )
      }).catch(err => {
        console.log(err);
      });

    } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
        'Your file is safe :)',
        'error'
      )
    }
  })
}

ngOnInit() {
    this.getProduct();
}

getProduct(){
  this.rest.getProduct().subscribe(data => {
    let productArr :any = this.rest.snapShotToArray(data);
    console.log(productArr.length);
    if(productArr.length === 0)
        this.loading = false;
    productArr.forEach(element => {
      let obj : any = {};
      obj = element;
      this.rest.getCategoryById(obj.category).subscribe(data => {
        let item :any = data;
        console.log(item)
        obj.categoryName = item.name;
        this.productArr.push(obj);
      });
      this.loading=false;

    });
  });
}

}
