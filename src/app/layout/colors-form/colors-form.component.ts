import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {RestService} from '../../rest.service';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-colors-form',
  templateUrl: './colors-form.component.html',
  styleUrls: ['./colors-form.component.scss']
})
export class ColorsFormComponent implements OnInit {

  constructor(private rest : RestService,private router : ActivatedRoute,private route :Router) { }

  loading = false;

  colorsForm = new FormGroup({
    color : new FormControl('', Validators.required),
    key : new FormControl()
  });

  save()
  {
    this.loading = true;
    let key = this.colorsForm.controls.key.value;
    if(key === null)
    {
      this.add();
    }
    else
    {
      this.update();
    }

  }
  add()
  {
    this.rest.addColor(this.colorsForm.value).then(success =>{
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  update()
  {
    this.rest.editColor(this.colorsForm.value).then(success => {
      this.loading = false;
      this.saveAlert();
    }).catch(err => {

    });
  }

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.colorsForm.patchValue(params);
    });
  }

  saveAlert() {
    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: 'Ok!',
    }).then((result) => {
      this.route.navigate(['/layout/colors']);

    })  }

}
