import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.scss']
})
export class SizeComponent implements OnInit {
sizeArr :any = [];
p: Number = 1;

loading = true;
  constructor(private rest :RestService,private route : Router) { }

  ngOnInit() {
    this.rest.getSizes().subscribe(data => {
      this.sizeArr = this.rest.snapShotToArray(data);
      this.loading = false;
    });
    
  }

  edit(item) {
      this.route.navigate(['/layout/size-form'],{queryParams : item});
  }

  delete(item) {
    // this.rest.deleteSize(item).then(success => {
    //   console.log(success);
    // }).catch(err => {
    //   console.log(err);
    // });

    Swal.fire({
      title: 'Successfully Updated!',
      // text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
          if (result.value) {
            this.rest.deleteSize(item).then(success => {
          console.log(success);
          Swal.fire(
            'Deleted!',
            // 'Your imaginary file has been deleted.',
            'success'
          )
        }).catch(err => {
          console.log(err);
        });
        
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
    })
  }

  
  
}
