import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireStorage} from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private afd : AngularFireDatabase,private afs : AngularFireStorage) { }

  addCategory(data) {
    return this.afd.list('category').push(data);
  }

  editCategory(data) {
      return this.afd.object('category/'+data.key).update({name : data.name});
  }

  getCategory() {
    return this.afd.list('category').snapshotChanges();
  }

  deleteCategory(data) {
    return this.afd.list('category').remove(data.key);
  }

  
  addSubCategory(data,category) {
    return this.afd.list('category/' + category + "/subcategory").push(data);
  }

  editSubCategory(data) {
      return this.afd.object('category/' + data.category + "/subcategory/" + data.key).set(data);
  }

  getSubCategory(category) {
    return this.afd.list('category/' + category + "/subcategory").snapshotChanges();
  }

  deleteSubCategory(data) {
    return this.afd.list('sub-category').remove(data.key);
  }

  addColor(data) {
    return this.afd.list('colors').push(data);
  }

  editColor(data) {
      return this.afd.object('colors/'+data.key).set(data);
  }

  getColors() {
    return this.afd.list('colors').snapshotChanges();
  }

  deleteColors(data) {
    return this.afd.list('colors').remove(data.key);
  }

  addSize(data) {
    return this.afd.list('size').push(data);
  }

  editSize(data) {
      return this.afd.object('size/'+data.key).set(data);
  }

  getSizes() {
    return this.afd.list('size').snapshotChanges();
  }

  deleteSize(data) {
    return this.afd.list('size').remove(data.key);
  }

  addProductBasic(data) {
    return this.afd.list('products').push(data);
  }
  editProductBasic(data) {
    return this.afd.object('products/'+data.key).update(data);
  }

  getProductBasic() {
    return this.afd.list('products').snapshotChanges();
  }
  addProduct(data) {
    return this.afd.list('products').push(data);
  }

  editProduct(data) {
      return this.afd.object('product/'+data.key).set(data);
  }

  getProduct() {
    return this.afd.list('products').snapshotChanges();
  }

  deleteProduct(data) {
    return this.afd.list('products').remove(data.key);
  }

  addProductDesc(data,key) {
    return this.afd.object('products/' + key + '/description').set(data);
  }

  editProductDesc(data, key) {
    return this.afd.object('products/' + key + '/description').set(data);
  }

  getProductDesc(key) {
    return this.afd.object('products/' + key + '/description').valueChanges();
  }

  addProductDetails(data,key) {
    return this.afd.object('products/' + key + '/details').set(data);
  }

  editProductDetails(data, key) {
    return this.afd.object('products/' + key + '/details').set(data);
  }
  
  getProductDetails(key) {
    return this.afd.object('products/' + key + '/details').valueChanges();
  }

  addImages(data,key) {
    return this.afd.list('products/' + key + '/images').push(data);

  }
  getImages(key)
  {
    return this.afd.list('/products/' + key + '/images').snapshotChanges();
  }
  deleteImg(key, src) {
    return this.afd.list('/products/' + key + '/images').remove(src.key);
  }

  getCategoryById(key) {
    return this.afd.object('category/' + key).valueChanges();
  }

  snapShotToArray(snapShot) {
    let retArr :any = [];

    snapShot.forEach(element => {
      let obj :any ={};
      obj = element.payload.val();
      obj.key = element.key;
      retArr.push(obj);
    });

    return retArr;
  }

  getProductByKey(key) {

    return this.afd.object('/products/' + key).valueChanges();
  }
}
